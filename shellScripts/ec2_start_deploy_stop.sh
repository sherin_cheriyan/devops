#!/bin/bash
export INSTANCE_ID=`aws ec2 describe-instances --filters "Name=tag:Name,Values=api-deployment-server" --query 'Reservations[].Instances[].InstanceId' --output text`
echo $INSTANCE_ID
aws ec2 start-instances --instance-ids $INSTANCE_ID || { echo "\nwait for some time and re-try\n"; exit 1; }

#waiting for the instane to start running
echo "Instance Starting..."
while INSTANCE_STATE=$(aws ec2 describe-instances --instance-ids $INSTANCE_ID --output text --query 'Reservations[*].Instances[*].State.Name'); test "$INSTANCE_STATE" = "pending"; do sleep 1; echo -n '.';  done
echo "\nInstance State: "$INSTANCE_STATE;

#waiting for the instance to be reachable status
INSTANCE_STATUS=`aws ec2 describe-instance-status --instance-id $INSTANCE_ID --output text --query 'InstanceStatuses[*].InstanceStatus.Status'`
echo "\nInstance Status : "$INSTANCE_STATUS;
SLEEP_TIME=1
while [ "$INSTANCE_STATUS" != "ok" ]
do
	sleep $SLEEP_TIME;
	echo -n '.';
	INSTANCE_STATUS=$(aws ec2 describe-instance-status --instance-id $INSTANCE_ID --output text --query 'InstanceStatuses[*].InstanceStatus.Status');
done 
echo "\nInstance Status : "$INSTANCE_STATUS;

#fetch instance IP for ssh
export INSTANCE_IP=`aws ec2 describe-instances --instance-ids $INSTANCE_ID --query 'Reservations[].Instances[].PublicIpAddress' --output text`
echo $INSTANCE_IP
ssh-keyscan -H $INSTANCE_IP >> ~/.ssh/known_hosts

#api_gateway_deployment
echo "Api-Gateway Deployment"
ssh $SSH_SERVER_USER@$INSTANCE_IP ./scripts/deploy_api_gateway.sh $BITBUCKET_BRANCH || { aws ec2 stop-instances --instance-ids $INSTANCE_ID; echo "\nThe Build was failed due to some errors. check the log\n";  exit 1; }

#stage code deployment and npm test
ssh $SSH_SERVER_USER@$INSTANCE_IP ./scripts/deploy.sh $BITBUCKET_BRANCH || { aws ec2 stop-instances --instance-ids $INSTANCE_ID; echo "\nThe Build was failed due to some errors. check the log\n";  exit 1; }

aws ec2 stop-instances --instance-ids $INSTANCE_ID
